package com.hangman;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class HangmanGameEngineTest {
	HangmanGameEngine classToTest;
	@Before
	public void setUp() throws Exception {
		classToTest = new HangmanGameEngine();
		classToTest.setSecretWord("Hello");
	}

	@Test
	public void testaddIncorectLetter() {
		boolean addIncorectLetter = classToTest.addIncorectLetter('l');
		assertFalse("Letter is a correct one", addIncorectLetter); 
		assertEquals("Tries still 0", 0, classToTest.getTries()); 
		addIncorectLetter = classToTest.addIncorectLetter('a');
		assertTrue("Letter is a incorect one", addIncorectLetter); 
		assertEquals("Tries at 1", 1, classToTest.getTries()); 
		addIncorectLetter = classToTest.addIncorectLetter('/');
		assertTrue("Letter is a incorect one", addIncorectLetter); 
		assertEquals("Tries at 2", 2, classToTest.getTries()); 
		addIncorectLetter = classToTest.addIncorectLetter(' ');
		assertTrue("Letter is a incorect one", addIncorectLetter); 
		assertEquals("Tries at 3", 3, classToTest.getTries()); 
	}

	@Test
	public void testDuplicateLetterWasInWarnedLetterList() {
        //classToTest.updateCorrectWordArrayWithLetter('o');
        //classToTest.duplicateLetterWasInWarnedLetterList('o');
		//TODO
		//correct letter 
		//correct letter second time
		//incorerct letter
		//crap (like ./*-+
	}
	
	@Test
	public void testupdateCorrectWordArrayWithLetter() {
		classToTest.updateCorrectWordArrayWithLetter('o');
		classToTest.duplicateLetterWasInWarnedLetterList('o');
		//TODO
		// letter not in word
		// letter multiple time in word
		//crap (like ./*-+
		//uppercase / lower case
	}
	
	@Test
	public void testLetterIsWordAlreadyFound() {
		//letterIsWordAlreadyFound
		classToTest.updateCorrectWordArrayWithLetter('o');
		classToTest.duplicateLetterWasInWarnedLetterList('o');
		//TODO
		// letter not in word
		// letter multiple time in word
		//crap (like ./*-+
		//uppercase / lower case
	}
	
	//handleSecretWordGuessInput
		//crap*--
		//wrong word increment
		//right word game won
	
	
	
}
