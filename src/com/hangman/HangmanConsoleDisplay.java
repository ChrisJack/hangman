package com.hangman;

import java.util.Scanner;

public class HangmanConsoleDisplay { //TODO implements HangmanDisplay
	private Scanner stdin = new Scanner(System.in); // for user input
	HangmanGameEngine engine = new HangmanGameEngine();
	
	public HangmanConsoleDisplay (){
		this(new HangmanGameEngine());
	}
	
	public HangmanConsoleDisplay (HangmanGameEngine engine){
		this.engine = engine;
	}
	
	public void playGame() {

		while (!engine.gameOver()) {

			// Print the Hangman picture
			printHangman();

			// Print the correct guesses in the secret word
			for (int i = 0; i < this.engine.getCorrectLetters().size(); i++){
				System.out.print(this.engine.getCorrectLetters().get(i) + " ");
			}
			// Print the incorrect letters that have been guessed
			System.out.print("\nWrong letters: ");
			for (int i = 0; i < this.engine.getIncorrectLetters().size(); i++){
				System.out.print(this.engine.getIncorrectLetters().get(i) + " ");
			}

			System.out.print("\n Press <1> pick a letter or <2> guess a word.: ");
			String input = stdin.nextLine();
			if (input.length() > 0){
				if (input.startsWith("1")){
					handleGuessLetter();
				} else if (input.startsWith("2")){
					displayGuessSecretWord();
				}
			}
		}

		System.out.println("The secret word was: " + engine.getSecretWord());
		if (engine.gameWon()) {
			System.out.println("Congratulations, you won!");
		} else {
			System.out.println("Sorry, you lost.");
			printHangman();
		}
	}

	private void handleGuessLetter(){
		// Prompt and read the next guess
		System.out.print("\nEnter a lower-case letter as your guess: ");
		String guess = stdin.nextLine();

		// Process the next guess
		if (guess.length() > 0){
			handleGuess(guess.charAt(0));
		}
	}
	
	private void handleGuess(char ch) {
		if (engine.isLetterInSecretWord(ch)){
			//manage entering a letter already entered
			hasAlreadyAskedTheLetter(ch);
			if (!engine.letterIsWordAlreadyFound(ch)){
				engine.updateCorrectWordArrayWithLetter(ch);
			} 
		} else {
			engine.addIncorectLetter(ch);
		}
	}
	
	private boolean hasAlreadyAskedTheLetter(char ch) {
		if (engine.letterIsWordAlreadyFound(ch)){
			boolean duplicateLetterWasInWarnedLetterList = engine.duplicateLetterWasInWarnedLetterList(ch);
			if (duplicateLetterWasInWarnedLetterList){
				System.out.println("We've already told you not to entered this letter again, we have no choice to remove one of you're body part");
			} else {
				System.out.println("[WARNING] You've already entered this letter, if you ask for that letter again you will lose a body part.");
			}
			return true;
		}
		return false;
	}
	
	private void displayGuessSecretWord(){
		// Prompt and read the next guess
		System.out.print("\nEnter what word you think is the secret word: ");
		String guess = stdin.nextLine();

		// Process the next guess
		engine.handleSecretWordGuessInput(guess);
	}
	
	public void printHangman() {
		printHangman(engine.getTries());
	}
	/**
	 * Print hangman method, currently tied to the HangmanGameEngine.Max_tries = 10 
	 * @param tries
	 */
	public void printHangman(int tries) {
		int poleLines = 9; // number of lines for hanging pole //6
		System.out.println("  _______");
		System.out.println("  |     |");

		int badGuesses = tries;//this.incorrectLetters.size();
		if (badGuesses == engine.getMaxTries()) {
			System.out.println("  |     |");
			System.out.println("  |     |");
		}

		if (badGuesses > 0) {
			if (badGuesses > 1){
				if (badGuesses == 2) {
					System.out.println("  |   (-  )");
					System.out.println("  |   (   )");
				} else if (badGuesses == 3) {
					System.out.println("  |   (- -)");
					System.out.println("  |   (   )");
				} else if (badGuesses == 4) {
					System.out.println("  |   (-.-)");
					System.out.println("  |   (   )");
				} else if (badGuesses >= 5) {
					System.out.println("  |   (-.-)");
					System.out.println("  |   ( o )");
				}
			} else {
				System.out.println("  |   (   )");
				System.out.println("  |   (   )");
			}
			poleLines = 7;//5
		}
		if (badGuesses > 5) {
			poleLines = 6;//4
			if (badGuesses == 6) {
				System.out.println("  |     |");
				System.out.println("  |     |");
			} else if (badGuesses == 7) {
				System.out.println("  |   \\ |");
				System.out.println("  |    \\|");
			} else if (badGuesses >= 8) {
				System.out.println("  |   \\ | /");
				System.out.println("  |    \\|/");
			}
		}
		if (badGuesses > 8) {
			poleLines = 4;//3
			if (badGuesses == 9) {
				System.out.println("  |    / ");
				System.out.println("  |   /");
			} else if (badGuesses >= 10) {
				System.out.println("  |    / \\");
				System.out.println("  |   /   \\");
			}
		}
		if (badGuesses == engine.getMaxTries()) {
			poleLines = 1;
		}

		for (int k = 0; k < poleLines; k++) {
			System.out.println("  |");
		}
		System.out.println("__|__");
		System.out.println();
	}
}
