package com.hangman;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class HangmanGameEngine {

	private Locale currentLocale;
	private static String[] words = // choose secret word from these
	{ "geography", "easy", "yesterday", "java", "paper", "opportunity",
			"shark", "plane", "transportation", "travel", "expedia", "cake",
			"remote", "pocket", "terminology", "website", "beer", "tool",
			"software", "spoon", "watermelon", "laptop", "toe", "cloud",
			"fundamental", "aboriginal", "absentmindedly", "absolute",
			"coincidence", "coherence", "biographical", "biometric",
			"biophysical", "bitmap", "boomerang", "dutiful", "duplication",
			"dumbfound", "earthshaking", "electrodynamic", "flash", "angular",
			"gift", "jquery", "json", "forensically", "foreshadowing",
			"hieroglyph", "milliseconds", "mindboggling", "capitol", "garbage",
			"anticipate", "apple", "abomination" };

	private String secretWord; // the chosen secret word
	private ArrayList<Character> correctLetters; // correct guesses
	private ArrayList<Character> incorrectLetters; // incorrect guesses
	private ArrayList<Character> warningForLetters; // remember letter that were
													// asked more that once
	private Boolean gameWon = null;
	public final static int MAX_TRIES = 10;
	private int tries = 0;

	public HangmanGameEngine() {
		this(Locale.ENGLISH);
	}

	public HangmanGameEngine(Locale chosenLocale) {

		currentLocale = chosenLocale;
		// Randomly choose a word from list of wordsF
		Random random = new Random();
		int index = random.nextInt(HangmanGameEngine.words.length);
		this.secretWord = HangmanGameEngine.words[index]; // TODO Word provider

		this.correctLetters = new ArrayList<Character>();
		for (int i = 0; i < this.secretWord.length(); i++)
			this.correctLetters.add('_');
		this.incorrectLetters = new ArrayList<Character>();
		this.warningForLetters = new ArrayList<Character>();
	}

	public ArrayList<Character> getCorrectLetters() {
		return correctLetters;
	}

	public void setCorrectLetters(ArrayList<Character> correctLetters) {
		this.correctLetters = correctLetters;
	}

	public ArrayList<Character> getIncorrectLetters() {
		return incorrectLetters;
	}

	public void setIncorrectLetters(ArrayList<Character> incorrectLetters) {
		this.incorrectLetters = incorrectLetters;
	}

	public int getTries() {
		return tries;
	}

	public int getMaxTries() {
		return MAX_TRIES;
	}

	public String getSecretWord() {
		return secretWord;
	}

	protected void setSecretWord(String secret) {
		secretWord = secret;
	}

	public Boolean gameWon() {
		return gameWon;
	}

	public Boolean gameLost() {
		return gameWon != null && !gameWon;
	}

	public Boolean gameOver() {
		return gameWon != null;
	}

	public boolean isLetterInSecretWord(char ch) {
		String lch = Character.toString(ch).toLowerCase(currentLocale);
		return secretWord.toLowerCase(currentLocale).contains(lch);
	}

	public int updateCorrectWordArrayWithLetter(char ch) {
		int letterfound = 0;
		if (isLetterInSecretWord(ch)) {
			int indexGuess = 0;
			while (indexGuess != -1) {
				char charInLowerCase = Character.toLowerCase(ch);
				indexGuess = secretWord.toLowerCase(currentLocale).indexOf(
						charInLowerCase, indexGuess);// TODO handle local
				if (indexGuess != -1) {
					correctLetters.set(indexGuess, charInLowerCase);
					indexGuess++;
					letterfound++;
				}
			}
		}
		return letterfound;
	}

	public boolean letterIsWordAlreadyFound(char ch) {
		char lch = Character.toLowerCase(ch);
		return correctLetters.contains(lch);
	}

	public boolean duplicateLetterWasInWarnedLetterList(char ch) {
		char lch = Character.toLowerCase(ch);
		if (warningForLetters.contains(lch)) {
			incrementTries();
			return true;
		} else {
			warningForLetters.add(lch);
			return false;
		}
	}

	public void handleSecretWordGuessInput(String guess) {
		if (secretWord.equalsIgnoreCase(guess)) {
			// Game won
			gameWon = true;
		} else {
			incrementTries();
		}
	}

	public boolean addIncorectLetter(char ch) {
		char charInLowerCase = Character.toLowerCase(ch);
		if (!isLetterInSecretWord(charInLowerCase)) {
			incorrectLetters.add(charInLowerCase);
			incrementTries();
			return true;
		}
		return false;
	}

	private void incrementTries() {
		tries++;
		if (tries > MAX_TRIES) {
			// Game lost
			gameWon = false;
		}
	}
}
